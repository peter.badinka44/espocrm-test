define('custom:get-lead-contacts', ['action-handler'], function (Dep) {

	return Dep.extend({

		lead: null, 
		contacts: [],
 
		getLeadContacts: async function (data, e) {
			this.lead = await Espo.Ajax.getRequest(`Lead/${this.view.model.id}`);

			Espo.Ajax.getRequest('Contact').then(response => {
				this.contacts = response.list.filter(
					contact => contact.emailAddress === this.lead.emailAddress
				);

				this.contacts.forEach(contact => {
					console.log(contact.name);
				});

			});
			
		},

	});
 });
